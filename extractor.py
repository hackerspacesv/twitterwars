import twitter
import json
import credentials
import os
import time
import random

api = twitter.Api(consumer_key=credentials.api_key,
                  consumer_secret=credentials.api_secret,
                  access_token_key=credentials.token,
                  access_token_secret=credentials.secret)

search_term = ""
loops = 20
batch_len = 100

statuses = []

latest_status = 0
break_on_id = 0

for i in range(0,loops):
  if latest_status is None:
    new_status = api.GetSearch(
      term=search_term,
      result_type='recent',
      count=100
    )
  else:
     new_status = api.GetSearch(
      term=search_term,
      result_type='recent',
      count=100,
      max_id=latest_status
    )
  
  if len(new_status)>0:
    last_status_idx = len(new_status)-1
    latest_status = new_status[last_status_idx].id
    statuses += new_status
  
  random_sleep = 2+random.random()*8 # Between 2 and 5 seconds to keep under rate limit
  time.sleep(random_sleep)

for status in statuses:
  if break_on_id is not None and status.id <= break_on_id:
      print("---BEGINING OF OLD EXTRACTION---")
      break;
  print(","+status.AsJsonString())

