import json
import os
import time
import random
import credentials
from datetime import datetime

# Initialize Random Seed
random.seed(credentials.random_seed)

# Opposition Hashtag Data
bd_filename = "./data/BukeleDictador.json"
ecb_filename = "./data/EstoyConBukele.json"
output_filename = "./data/tweets_consolidado_anonimizado.csv"


# Reference date to estimate account_age
date_to_compare = datetime.strptime(
        "Sat Apr 25 00:00:00 +0000 2020",
        "%a %b %d %H:%M:%S %z %Y"
    )

# BEGIN Helper functions
def OpenAndLoad(filename):
    json_file = open(filename)
    json_data = json.load(json_file)
    json_file.close()
    for status in json_data:
        status["filename"] = filename
    return json_data

def count_hashtag(ht_list, busqueda):
    ocurrencias = 0
    for hashtag in ht_list:
        if hashtag["text"].lower() == busqueda.lower():
            ocurrencias += 1
    return ocurrencias
# END Helper functions

print("Loading BukeleDictador datafile...")
data_bd = OpenAndLoad(bd_filename)
print("Loaded.")

print("Loading EstoyConBukele datafile...")
data_ecb = OpenAndLoad(ecb_filename)
print("Loaded.")

data = data_bd + data_ecb

# List of used random IDs.
random_status_list = []
random_user_list = []

def get_next_random(base_list):
    new_id = random.randrange(0,100000000)
    while new_id in base_list:
        print("+",end='')
        new_id = random.randrange(0,100000000)
    base_list.append(new_id)
    return new_id

# Memory maps with data
user_ids = {}
status_ids = {}
random_id_map = {}
print("Found {} total raw records".format(len(data)))
print("Anonimizing first level Ids".format(len(data)))
pos = 0
for status in data:
    pos += 1
    print("Processing record {} of {}".format(pos,len(data)))
    if status["id"] not in status_ids:
        status_datetime = datetime.strptime(
            status["created_at"],
            "%a %b %d %H:%M:%S %z %Y"
        )
        created_datetime = datetime.strptime(
                status["user"]["created_at"],
                "%a %b %d %H:%M:%S %z %Y"
                )
        if status["user"]["id"] not in user_ids:
            user_ids[status["user"]["id"]] = get_next_random(random_user_list)
        
        rnd_id = get_next_random(random_status_list)
        random_id_map[status["id"]] = rnd_id
        
        status_ids[status["id"]] = {
            "rnd_id": random_id_map[status["id"]],
            #"user_id": status["user"]["id"],
            "rnd_user_id": user_ids[status["user"]["id"]],
            "status_timestamp": status_datetime.timestamp()+random.randint(-900,900),
            "screen_name_len": len(status["user"]["screen_name"]),
            "numbers_in_screen_name": sum(map(lambda a: int(a.isdigit()), status["user"]["screen_name"])),
            "created_timestamp": created_datetime.timestamp()+random.randint(-172800,172800),
            "account_age": date_to_compare.timestamp()-created_datetime.timestamp(),
            "has_desc": 1 if "description" in status["user"] else 0,
            "desc_len": len(status["user"]["description"]) if "description" in status["user"] else 0,
            "favourites_count": status["user"]["favourites_count"] if "favourites_count" in status["user"] else 0,
            "followers_count": status["user"]["followers_count"] if "followers_count" in status["user"] else 0,
            "friends_count": status["user"]["friends_count"] if "friends_count" in status["user"] else 0,
            "statuses_count": status["user"]["statuses_count"],
            "text_len": len(status["text"]),
            "ht_bd": count_hashtag(status["hashtags"],"BukeleDictador"),
            "ht_ecb": count_hashtag(status["hashtags"],"EstoyConBukele"),
            "rt_count_acc": status["retweet_count"] if "retweet_count" in status else 0,
            "rt_count": 1 if "retweet_count" in status else 0,
            "rt_status_id": status["retweeted_status"]["id"] if "retweeted_status" in status else None,
            "rt_status_user": status["retweeted_status"]["user"]["id"] if "retweeted_status" in status else None,
            "reply_count": 1 if "in_reply_to_status_id" in status else 0,
            "reply_status_id": status["in_reply_to_status_id"] if "in_reply_to_status_id" in status else None,
            "reply_user_id": status["in_reply_to_user_id"] if "in_reply_to_user_id" in status else None,
            "filename": status["filename"],
            "seq_id": 0 #Just a placeholder
        }

output_file = open(output_filename,"w+")

FirstRow = True
pos = 0
total_records = len(status_ids)

print("Sorting data file...")
status_ids_sorted = sorted(status_ids)

print("Found {} total records. Printing to file".format(total_records))
print("Anonimizing second level Ids".format(len(data)))
for idx in status_ids_sorted:
    if FirstRow:
        header = "id"
        for column_name in status_ids[idx]:
            header += ","+column_name
        print("Writing header...")
        output_file.write(header+"\n")
        FirstRow = False
    
    status_ids[idx]['seq_id'] = pos
    # Second level anonimization for RT status ids
    if status_ids[idx]['rt_status_id'] is not None:
        if status_ids[idx]['rt_status_id'] in random_id_map:
            status_ids[idx]['rt_status_id'] = random_id_map[status_ids[idx]['rt_status_id']]
        else:
            new_id = get_next_random(random_status_list)
            random_id_map[status_ids[idx]['rt_status_id']] = new_id
            status_ids[idx]['rt_status_id'] = new_id
    
    # Second level anonimization for RT user ids
    if status_ids[idx]['rt_status_user'] is not None:
        if status_ids[idx]['rt_status_user'] in user_ids:
            status_ids[idx]['rt_status_user'] = user_ids[status_ids[idx]['rt_status_user']]
        else:
            new_id = get_next_random(random_user_list)
            status_ids[idx]['rt_status_user'] = new_id
            user_ids[status_ids[idx]['rt_status_user']] = new_id
    
    # Second level anonimization for Reply's status ids
    if status_ids[idx]['reply_status_id'] is not None:
        if status_ids[idx]['reply_status_id'] in random_id_map:
            status_ids[idx]['reply_status_id'] = random_id_map[status_ids[idx]['reply_status_id']]
        else:
            new_id = get_next_random(random_status_list)
            random_id_map[status_ids[idx]['reply_status_id']] = new_id
            status_ids[idx]['reply_status_id'] = new_id
            
    # Second level anonimization for Reply's user ids
    if status_ids[idx]['reply_user_id'] is not None:
        if status_ids[idx]['reply_user_id'] in user_ids:
            status_ids[idx]['reply_user_id'] = user_ids[status_ids[idx]['reply_user_id']]
        else:
            new_id = get_next_random(random_user_list)
            status_ids[idx]['reply_user_id'] = new_id
            user_ids[status_ids[idx]['reply_user_id']] = new_id
            
    # row = str(idx)
    row = "-1"
    for column_name in status_ids[idx]:
        row += ","+str(status_ids[idx][column_name])
    pos += 1
    print("Adding record {} of {}".format(pos,total_records))
    output_file.write(row+"\n")
output_file.close()
