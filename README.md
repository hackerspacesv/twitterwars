# Análisis de Interacciones en Twitter (Twitter Wars)

Este Jupyter Notebook incluye el análisis de interacciones realizado sobre los hashtags (HT) **#BukeleDictador** y **#EstoyConBukele** que se mantuvieron activos como *Trending Topic*(TT) en la red social Twitter desde de la media noche del sábado 18 de Abril de 2020 hasta el lunes 20 del mismo mes durante la mañana.

## Motivación

En el mundo virtual, las redes de sociales se han vuelto la principal fuente de información para muchísimos ciudadanos. Por la forma en que las redes funcionan las denominadas "tendencias" se originan de la viralización de ciertos contenidos que resultan de interés para la población.

Dentro de las redes se conoce como contenido "orgánico" a todo aquel contenido que se populariza a causa de interacciones entre usuarios. Este es un fenómeno emergente en que usuarios comienzan a compartir el contenido. A este mecanismo se le ha acuñado "viralización" ya que asemeja la forma en que los virus se dispersan en una población.

Sin embargo, siempre existe la duda si muchas de estas tendencias se originan de forma orgánica o si existen factores externos que podrían provocar que ciertos temas se viralizen a través de redes sociales. Uno de los mecanismos que se ha venido utilizando en los últimos años es la utilización de redes de *"influenciadores"* sumado a cuentas falsas para asemejar el comportamiento orgánico que tendría un contenido viral.

El presente análisis utiliza estos dos HT para tratar de determinar con ayuda de técnicas de análisis de datos si existen indicios de automatización en la forma en que se viralizaron ambos hashtags de recolección de datos.

## Recolección de Datos

Se utilizó la API estándar de Twitter para extraer los status retornados por la búsqueda de ambos hashtags.

La base de datos extrajo las menciones del Hashtag dentro del contenido del status, como respuestas a otros status o re-tweets que contenían dicho hashtag.

Para el HT **#BukeleDictador** se recolectaron tweets entre el ***Martes 13 de Abril a las 23:00*** y el ***Miércoles 22 de Abril a las 12:00*** hora de El Salvador.

Para el HT **#EstoyConBukele** se recolectaron tweets entre el ***Jueves 16 de Abril a las 03:00*** y el ***Miércoles 22 de Abril a las 12:00*** hora de El Salvador.

La viralización de ambos contenidos ocurrió cerca del sábado 18 a media noche, pero se recolectaron datos antes y después de la activación del HT para poder tener datos adicionales de comparación de la tendencia como referencia.


## Limitaciones de los Datos

Debido a que los datos se recolectaron utilizando la API estándar de Twitter tienen algunas limitaciones en su contenido:

1. No hay garantía de estos sean *todos* los tweets disponibles.
2. El contenido de los tweets está truncado a 150 caracteres.

Un análisis más detallado solo puede ser realizado con acceso Premium a la API de Twitter.

## Pre-procesamiento

Los status recolectados de Twitter son recibidos en formato JSON y contienen los siguientes datos:

* Información general del tweet: Fecha de creación, Texto, Hashtags utilizados y referencias multimedia.
* Información del usuario: Información publica del usuario (Información general disponible en el perfil)
* Si es un Re-tweet: Tweet al cual se hace referencia e información general del mismo.
* Si es una Respuesta: Información del tweet y el usuario al cual se responde.

Los datos enformato JSON fueron transformados en un archivo en formato CSV para su fácil procesamiento con herramientas de análisis de datos y se incluyó la siguiente información:

* **id**: ID único del estado. Generado por Twitter.
* **user_id**: ID único de usuario. Generado por Twitter.
* **status_timestamp**: Hora y fecha de publicación del tweet en formato de entero como *timestamp UNIX* hora de referencia GMT.
* **screen_name_len**: Longitud del nombre de usuario.
* **numbers_in_screen_name**: Total de dígitos en el nombre de usuario.
* **created_timestamp**: Hora y fecha de creación del usuario que publica el status en formato de entero como *timestamp UNIX* hora de referencia GMT.
* **account_age**: Diferencia en segundos entre el *timestamp UNIX GMT* para la fecha *Abril 25 2020 a las 00:00 horas* y el valor contenido en la columna **created_timestamp**.
* **has_desc**: Valor 1 si el usuario tiene una descripción en su cuenta, 0 en caso contrario.
* **desc_len**: Longitud en caracteres de la descripción de la cuenta.
* **favourites_count**: Conteo total de interacciones de "favoritos" en la cuenta.
* **friends_count**: Total de usuarios a los que sigue el usuario particular.
* **statuses_count**: Total de estados publicados por la cuenta desde el registro.
* **text_len**: Tamaño total de la interacción.
* **ht_bd**: Número de veces que utilizó el HT *#BukeleDictador*.
* **ht_ecb**: Número de veces que utilizó el HT *#EstoyConBukele*.
* **rt_count_acc**: Conteo de retweets. Este valor contiene el total de veces que un tweet fué re-twitteado al momento de la extracción.
* **rt_count**: 1 si el estado ha sido retwitteado.
* **rt_status_id**: ID de estado original del RT. Si este estado es el primero el valor es None.
* **rt_status_user**: ID del usuario original del RT. Si este estado es el primero el valor es None.
* **reply_count**: 1 si el estado es una respuesta.
* **reply_status_id**: ID del estado al que este tweet responde. None si no es respuesta. 
* **reply_user_id**: ID del usuario al que este tweet responde. None si no es respuesta.
* **filename**: Nombre del archivo original del cual se procesaron los datos.


## Anonimización

Aunque los Términos de Servicio de Twitter permiten compartir los IDs de estados y de usuarios con fines de análisis. Durante la semana un funcionario de gobierno publicó un website donde se presentaban analíticas que rastreaban a usuarios que habían utilizado el HT *#BukeleDictador*.

La utilización de datos de Twitter para realizar rastreo de usuarios está prohibido según las políticas de uso. Debido a que existe el potencial de que al compartir la data cruda, o incluso los IDs de interacciones se pueda utilizar estos datos con fines de rastreo se ha anonimizado el conjunto de datos para que pueda ser utilizado en análisis reduciendo el riesgo de rastreo de usuarios.

Las transformaciones aplicadas al conjunto de datos son los siguientes:
    
* **id**: El ID original del estado de Twitter ha sido reemplazado por la columna **rnd_id**. Este es un ID aleatorio único generado para cada estado. A diferencia del ID de Twitter, este no es secuencial.
* **user_id**: El ID original de usuario de Twitter ha sido reemplazado por la columna **rnd_user_id**. Este es un ID aleatorio único generado para cada usuario. A diferencia del ID de Twitter, este ID no es secuencial.
* **status_timestamp**: Al estado original se le ha agregado **al azar** un incremento o retardo de +/-15 minutos. De tal manera que no es posible obtener la hora de publicación exacta.
* **created_timestamp**: A la fecha de creación original se le ha agregado **al azar** un incremento o retardo de +/- 48h. De tal manera que no es posible obtener la fecha de registro exacto del usuario.
* Se agregó la columna **seq_id** que indica si el tweet fue publicado antes o despues que otro tweet en la secuencia.

Para las columnas **rt_status_id**, **rt_status_user**, **reply_status_id** y **reply_user_id** se reemplazó el ID original por el correspondiente **rnd_id** o **rnd_user_id**. Así que debería ser posible hacer análisis de red con el conjunto de datos anonimizado.

### Limitantes en el mecanismo de anonimización

Un atacante con recursos podría utilizar la meta-data para buscar y clasificar los tweets que coincidan con la descripción genérica de los tweets anonimizados. Sin embargo el mecanismo de anonimización reduce el nivel de certeza para el atacante.

La API estándar de Twitter solo permite realizar búsquedas de estatus que no tengan más de 1 semana de haber sido publicados. Esta base de datos se publica exáctamente 1 semana después del evento analizado de tal manera que usuarios con acceso estándar a la API no podrán utilizar el mecanismo de búsquedas para identificar los tweets originales.

Siempre es posible que usuarios con acceso premium a la API puedan realizar esta clase de búsquedas. Pero tomando en consideración que existen condiciones de servicio más estrictas de uso para usuarios de la API Premium y bajo el riesgo de terminación de servicio por incumplimiento de los Términos de Uso. Esperamos que este mecanismo de anonimización sea disuasivo suficiente para la gran mayoría de atacantes.

El mecanismo de anonimización seleccionado también agrega un poco de ruido a los datos. Sin embargo al ser un ruido agregado al azar se espera que los valores medios se mantengan constantes y estos puedan seguir siendo utilizados para análisis.

## Datos

Los datos para replicar este análisis pueden ser descargados de la siguiente dirección:

[https://portal.sivardata.com/dataset/twitterwars](https://portal.sivardata.com/dataset/twitterwars)

## Copyrights

**Código Fuente**

*Copyright 2020 Mario Gómez @ Hackerspace San Salvador*

Usted puede copiar, modificar y redistribuir el código fuente incluído con este análisis bajo las condiciones de la licencia [GNU/GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) o (a su elección) una versión posterior. Deberá incluír esta noticia de Copyright. El código fuente se ofrece sin ningúna garantía, nisiquiera con la garantía implicita de que este código cumple con su funcionamiento.

**Textos y gráficos**

*Copyright 2020 Mario Gómez @ Hackerspace San Salvador*

Usted puede copiar, modificar y redistribuir los textos y gráficos incluídos en este análisis bajo la licencia Creative Commons - Attribution-Share-Alike 4.0. Puede encontrar los términos de licencia en el [sitio web de Creative Commons](https://creativecommons.org/licenses/by-sa/4.0/).

**Datos**

En cumplimiento a los Términos de Uso de Twitter se comparten datos de forma completamente anonimizada. Esta licencia solo es aplicable a los datos procesados y anonimizados compartidos junto a este análisis. Si desea acceso a los datos crudos por favor solicitarlos a [mario.gomez@teubi.co](mailto:mario.gomez@teubi.co).

*Copyright 2020 Mario Gómez @ Hackerspace San Salvador*

La base de datos anonimizada de **Twitter Wars** se hace disponible a través de la [Open Database Licence](http://opendatacommons.org/licenses/odbl/1.0/). Todos los derechos en los contenidos individuales de esta base de datos estan licenciados bajo la [Database Contents License](http://opendatacommons.org/licenses/dbcl/1.0/).
